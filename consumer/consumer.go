package consumer

import (
	"context"
	"log"
	"os"
	"sync"
	"time"

	"github.com/IBM/sarama"
	"github.com/pkg/errors"
	"gitlab.com/snarksliveshere/kafka-batch-lib"
)

const (
	defaultMaxDBInsertQty    = 200
	defaultTimeToCollectPack = 500 * time.Millisecond
	readMsgsDebugInterval    = time.Minute
)

type eventConsumer struct {
	kafkaCgSession sarama.ConsumerGroupSession

	consumerTimeoutCtx       context.Context
	consumerTimeoutCtxCancel context.CancelFunc

	consumerCancelCtx       context.Context
	consumerCancelCtxCancel context.CancelFunc

	consumerProcessMux sync.RWMutex

	copiedEvents map[string][]interface{}

	copiedQueueMsgForAck map[string][]*sarama.ConsumerMessage
	mapCollectedEvents   map[string]*collectedEvents

	entitiesToConsume []string

	maxCollectPackQtyFunc  func() int
	maxCollectPackQty      int
	timeForCollectPackFunc func() time.Duration
	processLifetimeSecFunc func() time.Duration

	eventHandler EventHandler
}

type EventHandler interface {
	ExtractEventFromMsg(ctx context.Context, topic string, msg []byte) (string, interface{}, error)
	ProcessEvents(ctx context.Context, copiedEvents map[string][]interface{}) map[string]error
}

type collectedEvents struct {
	eventMutex *sync.Mutex
	collection []interface{}
	acks       []*sarama.ConsumerMessage
}

type Consumer struct {
	eventConsumer *eventConsumer
	consumerGroup sarama.ConsumerGroup
	topics        []string
}

func NewConsumer(
	client sarama.Client,
	entities []string,
	collectDurationFn func() time.Duration,
	maxPackCollectQtyFn func() int,
	processLifetimeSecFn func() time.Duration,
	handler EventHandler,
	consumerGroup string,
) (*Consumer, error) {
	consumer := &eventConsumer{
		eventHandler:           handler,
		copiedEvents:           make(map[string][]interface{}),
		copiedQueueMsgForAck:   make(map[string][]*sarama.ConsumerMessage),
		mapCollectedEvents:     make(map[string]*collectedEvents),
		processLifetimeSecFunc: processLifetimeSecFn,
		maxCollectPackQtyFunc:  maxPackCollectQtyFn,
		timeForCollectPackFunc: collectDurationFn,
	}
	consumer.setMaxCollectPackQty()

	if err := consumer.initEntities(entities); err != nil {
		return nil, errors.Wrap(err, "initEntities")
	}

	cg, err := sarama.NewConsumerGroupFromClient(consumerGroup, client)
	if err != nil {
		return nil, errors.Wrap(err, "newConsumerGroup")
	}

	return &Consumer{eventConsumer: consumer, consumerGroup: cg, topics: entities}, nil
}

func (c *Consumer) Run(ctx context.Context) error {
	startControlPlane(ctx, c.eventConsumer)
	listenQueue(ctx, c.consumerGroup, c.topics, c.eventConsumer)
	return nil
}

func startControlPlane(ctx context.Context, consumer *eventConsumer) {
	consumer.initCollectedEvents()
	kafka_batch_lib.RetryGoroutine(time.Second, func(mux *sync.Mutex) {
		go func() {
			defer mux.Unlock()
			defer kafka_batch_lib.Recover(ctx, nil)
			log.Println(ctx, "run control plane")
			consumer.controlPlane(ctx)
		}()
	})
}

func listenQueue(
	ctx context.Context,
	consumerGroup sarama.ConsumerGroup,
	topics []string,
	consumerGroupHandler sarama.ConsumerGroupHandler,
) {
	log.Println(ctx, "start to listen queue")
	for {
		err := consumerGroup.Consume(ctx, topics, consumerGroupHandler)
		if err != nil {
			log.Printf("listenQueue, err:[%s]", err.Error())
		}
	}
}

func (c *eventConsumer) initCollectedEvents() {
	for _, v := range c.entitiesToConsume {
		c.mapCollectedEvents[v].collection = make([]interface{}, 0, c.maxCollectPackQty)
		c.mapCollectedEvents[v].acks = make([]*sarama.ConsumerMessage, 0, c.maxCollectPackQty)
	}
}

func (c *eventConsumer) Setup(cgSession sarama.ConsumerGroupSession) error {
	log.Println(cgSession.Context(), "setup consumer group handler")
	c.kafkaCgSession = cgSession
	return nil
}

func (c *eventConsumer) Cleanup(cgSession sarama.ConsumerGroupSession) error {
	log.Println(cgSession.Context(), "cleanup consumer group handler")
	return nil
}

func (c *eventConsumer) ConsumeClaim(cgSession sarama.ConsumerGroupSession, cgClaim sarama.ConsumerGroupClaim) error {
	defer kafka_batch_lib.Recover(cgSession.Context(), nil)

	var eventSize uint64
	defer logReadMsgsFromPart(cgSession, cgClaim, eventSize)

	logStartTime := time.Now()
	for {
		if time.Now().After(logStartTime.Add(readMsgsDebugInterval)) {
			logReadMsgsFromPart(cgSession, cgClaim, eventSize)
			logStartTime, eventSize = time.Now(), 0
		}

		needContinue, err := c.collectQueueEvent(cgSession, cgClaim)
		eventSize++
		if err != nil {
			log.Println(cgSession.Context(), err)
			return errors.Wrap(err, "collectQueueEvent")
		}
		if !needContinue {
			return nil
		}
	}
}

func (c *eventConsumer) getTimeForCollectPack() time.Duration {
	t := c.timeForCollectPackFunc()
	if t == time.Duration(0) {
		return defaultTimeToCollectPack
	}
	return t
}

func (c *eventConsumer) setMaxCollectPackQty() {
	c.maxCollectPackQty = c.maxCollectPackQtyFunc()
	if c.maxCollectPackQty == 0 {
		c.maxCollectPackQty = defaultMaxDBInsertQty
	}
}

func (c *eventConsumer) initEntities(entities []string) error {
	if len(entities) == 0 {
		return errors.New("no entities to consume")
	}
	c.entitiesToConsume = entities
	for _, v := range entities {
		str := collectedEvents{
			eventMutex: &sync.Mutex{},
		}
		c.mapCollectedEvents[v] = &str
	}
	return nil
}

func (c *eventConsumer) collectQueueEvent(
	cgSession sarama.ConsumerGroupSession,
	cgClaim sarama.ConsumerGroupClaim,
) (
	needContinue bool,
	err error,
) {
	c.consumerProcessMux.RLock()
	defer c.consumerProcessMux.RUnlock()

	select {
	case msg, ok := <-cgClaim.Messages():
		if !ok {
			return false, nil
		}

		entity, event, err := c.eventHandler.ExtractEventFromMsg(cgSession.Context(), msg.Topic, msg.Value)
		if err != nil {
			return false, errors.Wrap(err, "ExtractEventFromMsg")
		}

		if entity != "" {
			c.addEvent(entity, event, msg)
		} else {
			c.markMsg(msg)
		}
		return true, nil
	case <-c.consumerCancelCtx.Done():
		return true, nil
	case <-cgSession.Context().Done():
		return false, nil
	}
}

func (c *eventConsumer) addEvent(entity string, event interface{}, msg *sarama.ConsumerMessage) {
	events, ok := c.mapCollectedEvents[entity]
	if !ok {
		log.Printf("entity {%s} doesn't support", entity)
		return
	}

	events.eventMutex.Lock()
	defer events.eventMutex.Unlock()
	if len(events.collection) < c.maxCollectPackQty {
		events.collection = append(events.collection, event)
		events.acks = append(events.acks, msg)
	} else {
		c.consumerTimeoutCtxCancel()
	}
}

func (c *eventConsumer) controlPlane(ctx context.Context) {
	startTime := time.Now().UTC()
	c.initNewConsumerContexts(ctx)
	c.setMaxCollectPackQty()
	for {
		processLifetime := c.processLifetimeSecFunc()
		if processLifetime != 0 && startTime.Add(processLifetime).Before(time.Now().UTC()) {
			c.consumerCancelCtxCancel()
			log.Println(ctx, "process has expired, finish work")
			os.Exit(0)
		}
		c.waitCollectedEvents()
		c.consumerCancelCtxCancel()
		c.setMaxCollectPackQty()

		c.copyAndClearCollectedEvents()
		c.initNewConsumerContexts(ctx)

		if c.getCopiedEventsLen() == 0 {
			continue
		}
		eventsSum := 0
		for entity, events := range c.copiedEvents {
			eventsNum := len(events)
			eventsSum += eventsNum
			log.Printf("-----> Start processing '%s' events: %d", entity, eventsNum)
		}
		errorMap := c.eventHandler.ProcessEvents(ctx, c.copiedEvents)
		log.Printf("<----- Finish processing of all events: %d", eventsSum)
		c.ackQueueKafkaMsg(ctx, errorMap)
		log.Printf("Ack queue messages: %d", eventsSum)
	}
}

func (c *eventConsumer) initNewConsumerContexts(ctx context.Context) {
	c.consumerProcessMux.Lock()
	defer c.consumerProcessMux.Unlock()

	if c.consumerTimeoutCtxCancel != nil {
		c.consumerTimeoutCtxCancel()
	}

	if c.consumerCancelCtxCancel != nil {
		c.consumerCancelCtxCancel()
	}

	c.consumerTimeoutCtx, c.consumerTimeoutCtxCancel = context.WithTimeout(ctx, c.getTimeForCollectPack())
	c.consumerCancelCtx, c.consumerCancelCtxCancel = context.WithCancel(ctx)
}

func (c *eventConsumer) waitCollectedEvents() {
	<-c.consumerTimeoutCtx.Done()
}

func (c *eventConsumer) copyCollectedEvents() {
	for _, v := range c.entitiesToConsume {
		c.copiedEvents[v] = append([]interface{}(nil), c.mapCollectedEvents[v].collection...)
		c.copiedQueueMsgForAck[v] = append([]*sarama.ConsumerMessage(nil), c.mapCollectedEvents[v].acks...)
	}
}

func (c *eventConsumer) copyAndClearCollectedEvents() {
	for _, topic := range c.entitiesToConsume {
		event, ok := c.mapCollectedEvents[topic]
		if !ok {
			log.Printf("topic entity {%s} doesn't support", topic)
			return
		}
		event.eventMutex.Lock()

		c.copiedEvents[topic] = append([]interface{}(nil), event.collection...)
		c.copiedQueueMsgForAck[topic] = append([]*sarama.ConsumerMessage(nil), event.acks...)

		event.collection = make([]interface{}, 0, c.maxCollectPackQty)
		event.acks = make([]*sarama.ConsumerMessage, 0, c.maxCollectPackQty)

		event.eventMutex.Unlock()
	}
}

func (c *eventConsumer) getCopiedEventsLen() int {
	var ln int
	for _, v := range c.entitiesToConsume {
		ln += len(c.copiedEvents[v])
	}
	return ln
}

func (c *eventConsumer) ackQueueKafkaMsg(ctx context.Context, errorMap map[string]error) {
	if len(errorMap) == 0 {
		return
	}
	for v, err := range errorMap {
		if err != nil {
			log.Println(ctx, err)
			continue
		}
		c.ackEntityMsg(v)
	}
}

func (c *eventConsumer) ackEntityMsg(entity string) {
	if len(c.copiedQueueMsgForAck[entity]) == 0 {
		return
	}
	for i := 0; i < len(c.copiedQueueMsgForAck[entity]); i++ {
		c.markMsg(c.copiedQueueMsgForAck[entity][i])
	}
	c.copiedQueueMsgForAck[entity] = make([]*sarama.ConsumerMessage, 0, c.maxCollectPackQty)
}

func (c *eventConsumer) markMsg(msg *sarama.ConsumerMessage) {
	c.kafkaCgSession.MarkMessage(msg, "")
}

func logReadMsgsFromPart(_ sarama.ConsumerGroupSession, cgClaim sarama.ConsumerGroupClaim, eventSize uint64) {
	log.Printf("Read messages from partition#%d: %d", cgClaim.Partition(), eventSize)
}
