package kafka_batch_lib

import (
	"context"
	"fmt"
	"log"
	"runtime/debug"
	"strings"
	"sync"
	"time"
)

func Recover(ctx context.Context, fn func(err error)) {
	if p := recover(); p != nil {
		err := fmt.Errorf("recovered from panic %v", p)
		log.Println(ctx, err)

		stack := strings.Split(string(debug.Stack()), "\n")
		log.Printf(fmt.Sprintf("%v", p), "stack_trace", stack)

		if fn != nil {
			fn(err)
		}
	}
}

// RetryGoroutine for an endless retry of goroutine after a panic
func RetryGoroutine(delay time.Duration, fn func(mux *sync.Mutex)) {
	go func() {
		var mux sync.Mutex
		for {
			mux.Lock()
			fn(&mux) // vs: need to run "defer mux.Unlock()" inside
			time.Sleep(delay)
		}
	}()
}
