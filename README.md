### kafka-batch-lib
***
либа для сбора событий в пачку из кафки. \
далее обрабатываем их на стороне приложения.\
Методы обработчика
```
type EventHandler interface {
	ExtractEventFromMsg(ctx context.Context, topic string, msg []byte) (string, interface{}, error)
	ProcessEvents(ctx context.Context, copiedEvents map[string][]interface{}) map[string]error
}
```
####ExtractEventFromMsg
вытаскиваем необходимое нам событие из топика \
собираем в мапу с ключом entity - наш идентификатор события

####ProcessEvents
обработка собранных событий, к примеру, закидываем в базу

***
#### инициализация
```
func NewConsumer(
	ctx context.Context,
	cfg Config,
	entities []string,
	collectDurationFn func() time.Duration,
	maxPackCollectQtyFn func() int,
	processLifetimeSecFn func() time.Duration,
	handler EventHandler,
)

Config - конфиг для самой кафки, топик, брокеры etc

entities - агрегируем события по признаку в мапе и даем ему условное имя
Управляем этой логикой в ExtractEventFromMsg
т.е. в упрощенном варианте ExtractEventFromMsg может начинаться как-то так
if bytes.Contains(msg, []byte("facebook")) {...}
либо просто разделяем по топику

consumer_collect_duration:
usage: "consumer duration of collection bulk of events (millisecond)"
время, по истечению которого, передаем пачку событий в процессинг

consumer_max_db_insert_qty:
usage: "max size of events rows for insert to postgres"
размер пачки, по достижении которой передаем пачку событий в процессинг

process_life_time_sec:
usage: live time of cron process
если мы хотим тормознуть вручную. Если не хотим, передаем 0
```
